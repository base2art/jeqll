package com.base2art.jeqll.impl;

import com.base2art.jeqll.projections.IField;
import com.base2art.jeqll.util.ReflectionUtil;
import java.util.Iterator;


public class IterableQuerySelectProjectable<TObject, TProjectable> implements Iterable<TProjectable>
{
  private final Iterable<TObject> query;
  private final IField<TProjectable> field;

  public IterableQuerySelectProjectable(Iterable<TObject> query, IField<TProjectable> projectable)
  {
    this.query = query;
    this.field = projectable;

  }

  @Override
  public Iterator<TProjectable> iterator()
  {
    return new IterableQueryProjectableIterator(this);
  }

  private class IterableQueryProjectableIterator implements Iterator<TProjectable>
  {
    private final IterableQuerySelectProjectable<TObject, TProjectable> iterable;
    private final Iterator<TObject> innerIterator;
    
    public IterableQueryProjectableIterator(IterableQuerySelectProjectable<TObject, TProjectable> iterable)
    {
      this.iterable = iterable;
      this.innerIterator = iterable.query.iterator();
    }

    @Override
    public boolean hasNext()
    {
      return this.innerIterator.hasNext();
    }

    @Override
    public TProjectable next()
    {
      return this.convert(this.innerIterator.next());
    }

    @Override
    public void remove()
    {
      throw new UnsupportedOperationException("Not supported yet.");
    }

    private TProjectable convert(TObject next)
    {
      return ReflectionUtil.<TProjectable>field(next, this.iterable.field.getName());
    }
  }
}
