package com.base2art.jeqll.impl;

import com.base2art.jeqll.util.IPair;
import com.base2art.jeqll.IQuery;
import com.base2art.jeqll.util.ImmutablePair;
import com.base2art.jeqll.projections.IField;
import com.base2art.jeqll.util.ReflectionUtil;
import java.util.ArrayList;
import java.util.Iterator;

public class IterableQueryGroupable<TKey extends Comparable<TKey>, TObject>
  implements Iterable<IPair<TKey, IQuery<TObject>>>
{

  private final Iterable<TObject> query;
  private final IField<TKey> field;
  private Class<TObject> klazz;

  public IterableQueryGroupable(Iterable<TObject> query, IField<TKey> projectable, Class<TObject> klazz)
  {
    this.query = query;
    this.field = projectable;
    this.klazz = klazz;

  }

  @Override
  public Iterator<IPair<TKey, IQuery<TObject>>> iterator()
  {
    return new IterableQueryGroupableIterator(this);
  }

  private class IterableQueryGroupableIterator implements Iterator<IPair<TKey, IQuery<TObject>>>
  {

    private final IterableQueryGroupable<TKey, TObject> iterable;
//    private final Iterator<TObject> innerIterator;
    private Iterator<IPair<TKey, IQuery<TObject>>> keyIterator;

    public IterableQueryGroupableIterator(IterableQueryGroupable<TKey, TObject> iterable)
    {
      this.iterable = iterable;
//      this.innerIterator = iterable.query.iterator();
    }

    @Override
    public boolean hasNext()
    {
      if (this.keyIterator == null)
      {
        this.keyIterator = this.createIterator();
      }

      return this.keyIterator.hasNext();
    }

    @Override
    public IPair<TKey, IQuery<TObject>> next()
    {
      if (this.keyIterator == null)
      {
        this.keyIterator = this.createIterator();
      }

      return this.keyIterator.next();
    }

    @Override
    public void remove()
    {
      throw new UnsupportedOperationException("Not supported yet.");
    }

    private Iterator<IPair<TKey, IQuery<TObject>>> createIterator()
    {
      ArrayList<ImmutablePair<TKey, ArrayList<TObject>>> pairings = new ArrayList<>();
      for (TObject obj : this.iterable.query)
      {
        TKey objKey = this.convert(obj);
        ImmutablePair<TKey, ArrayList<TObject>> selected = null;
        for (ImmutablePair<TKey, ArrayList<TObject>> pairing : pairings)
        {
          if (pairing.getKey().compareTo(objKey) == 0)
          {
            selected = pairing;
          }
        }

        if (selected == null)
        {
          selected = new ImmutablePair<>(objKey, new ArrayList<TObject>());
          pairings.add(selected);
        }

        selected.getValue().add(obj);
      }

      ArrayList<IPair<TKey, IQuery<TObject>>> result = new ArrayList<>();
      for (ImmutablePair<TKey, ArrayList<TObject>> immutablePair : pairings)
      {
        result.add(
          new ImmutablePair<TKey, IQuery<TObject>>(
          immutablePair.getKey(),
          new IterableQuery<TObject>(immutablePair.getValue(), this.iterable.klazz)));
      }

      return result.iterator();
    }

    private TKey convert(TObject next)
    {
      return ReflectionUtil.<TKey>field(next, this.iterable.field.getName());
    }
  }
}
