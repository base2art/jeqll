package com.base2art.jeqll.impl;

import com.base2art.jeqll.matchers.IMatcher;
import java.util.Iterator;

public class IterableQueryIterable<T> implements Iterable<T>
{

  private final Iterable<T> list;
  private final IMatcher matcher;

  public IterableQueryIterable(Iterable<T> list, IMatcher matcher)
  {
    this.list = list;
    this.matcher = matcher;
  }

  @Override
  public Iterator<T> iterator()
  {
    return new IterableQueryIterator<>(list, matcher);
  }
}
