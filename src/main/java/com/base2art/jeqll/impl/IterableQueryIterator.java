package com.base2art.jeqll.impl;

import com.base2art.jeqll.matchers.IMatcher;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class IterableQueryIterator<T> implements Iterator<T>
{

  private final Iterator<T> iterable;
  private final IMatcher matcher;
  private T nextItem;
  private boolean hasNextItem;
  private int i;

  public IterableQueryIterator(Iterable<T> iterable, IMatcher matcher)
  {
    this.iterable = iterable.iterator();
    this.matcher = matcher;
    this.i = -1;
  }

  @Override
  public boolean hasNext()
  {
    while (this.iterable.hasNext())
    {
      this.nextItem = this.iterable.next();
      this.i += 1;
      if (this.matcher.isMatch(this.nextItem, i))
      {
        this.hasNextItem = true;
        return true;
      }
    }

    this.hasNextItem = false;
    return false;
  }

  @Override
  public T next()
  {
    if (this.hasNextItem)
    {
      return this.nextItem;
    }

    throw new NoSuchElementException();
  }

  @Override
  public void remove()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
