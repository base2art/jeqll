package com.base2art.jeqll.impl;

import java.util.Iterator;

public class ArrayIterable<T> implements Iterable<T>
{

  private final T[] values;

  public ArrayIterable(T[] values)
  {
    this.values = values;

  }

  @Override
  public Iterator<T> iterator()
  {
    return new ArrayIterator(values);
  }

  private class ArrayIterator implements Iterator<T>
  {

    private final T[] values;
    private int i = -1;

    public ArrayIterator(T[] values)
    {
      this.values = values;
    }

    @Override
    public boolean hasNext()
    {
      i += 1;
      if (i < values.length)
      {
        return true;
      }

      return false;
    }

    @Override
    public T next()
    {
      if (0 <= i && i < values.length)
      {
        return this.values[i];
      }

      throw new java.util.NoSuchElementException("Called next when can't call next.");
    }

    @Override
    public void remove()
    {
      throw new UnsupportedOperationException("Not supported yet.");
    }
  }
}
