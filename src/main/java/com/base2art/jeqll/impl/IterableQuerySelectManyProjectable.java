package com.base2art.jeqll.impl;

import com.base2art.jeqll.projections.IField;
import com.base2art.jeqll.util.ReflectionUtil;
import java.util.Iterator;

public class IterableQuerySelectManyProjectable<TObject, TProjectable>
  implements Iterable<TProjectable>
{

  private final Iterable<TObject> query;
  private final IField<TProjectable> field;

  public IterableQuerySelectManyProjectable(Iterable<TObject> query, IField<TProjectable> projectable)
  {
    this.query = query;
    this.field = projectable;

  }

  @Override
  public Iterator<TProjectable> iterator()
  {
    return new IterableQueryProjectableIterator(this);
  }

  private class IterableQueryProjectableIterator implements Iterator<TProjectable>
  {

    private final IterableQuerySelectManyProjectable<TObject, TProjectable> iterable;
    private final Iterator<TObject> innerIterator;
    private Iterator<TProjectable> last;

    public IterableQueryProjectableIterator(IterableQuerySelectManyProjectable<TObject, TProjectable> iterable)
    {
      this.iterable = iterable;
      this.innerIterator = iterable.query.iterator();
    }

    @Override
    public boolean hasNext()
    {
      if (this.last == null)
      {
        if (!this.innerIterator.hasNext())
        {
          return false;
        }

        this.last = this.convert(this.innerIterator.next()).iterator();
      }

      if (!this.last.hasNext())
      {
        this.last = null;
        return this.hasNext();
      }

      return true;
    }

    @Override
    public TProjectable next()
    {
      return this.last.next();
    }

    @Override
    public void remove()
    {
      throw new UnsupportedOperationException("Not supported yet.");
    }

    private Iterable<TProjectable> convert(TObject next)
    {
      final String name = this.iterable.field.getName();
      Object obj = ReflectionUtil.<Object>field(next, name);
      try
      {
        if (obj instanceof Object[])
        {
          return new ArrayIterable<TProjectable>((TProjectable[]) obj);
        }

        return (Iterable<TProjectable>) obj;
      }
      catch (ClassCastException cce)
      {
        throw new java.util.UnknownFormatConversionException(name);
      }
    }
  }
}
