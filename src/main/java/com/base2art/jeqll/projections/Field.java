package com.base2art.jeqll.projections;

public class Field<T>
  implements IField<T>
{

  private final String name;
  private final Class<T> klazz;

  public Field(String name, Class<T> klazz)
  {
    this.name = name;
    this.klazz = klazz;
  }

  @Override
  public String getName()
  {
    return this.name;
  }

  @Override
  public Class<T> getKlazz()
  {
    return this.klazz;
  }
}
