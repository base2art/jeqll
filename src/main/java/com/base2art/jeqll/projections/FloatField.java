package com.base2art.jeqll.projections;

public class FloatField extends Field<Float> implements IArithmaticField<Float>
{

  public FloatField(String name)
  {
    super(name, Float.class);
  }

  @Override
  public Float add(Float first, Float other)
  {
    return first + other;
  }

  @Override
  public Float subtract(Float first, Float other)
  {
    return first - other;
  }

  @Override
  public Float multiply(Float first, Float other)
  {
    return first * other;
  }

  @Override
  public Float divideBy(Float first, Float other)
  {
    return first / other;
  }

  @Override
  public Float divideByInt(Float first, int other)
  {
    return first / other;
  }
}
