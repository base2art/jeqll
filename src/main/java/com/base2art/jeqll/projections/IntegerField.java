package com.base2art.jeqll.projections;

public class IntegerField extends Field<Integer> implements IArithmaticField<Integer>
{

  public IntegerField(String name)
  {
    super(name, Integer.class);
  }

  @Override
  public Integer add(Integer first, Integer other)
  {
    return first + other;
  }

  @Override
  public Integer subtract(Integer first, Integer other)
  {
    return first - other;
  }

  @Override
  public Integer multiply(Integer first, Integer other)
  {
    return first * other;
  }

  @Override
  public Integer divideBy(Integer first, Integer other)
  {
    return first / other;
  }

  @Override
  public Integer divideByInt(Integer first, int other)
  {
    return first / other;
  }
}
