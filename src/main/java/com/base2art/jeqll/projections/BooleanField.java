package com.base2art.jeqll.projections;

public class BooleanField extends Field<Boolean>
{

  public BooleanField(String name)
  {
    super(name, Boolean.class);
  }
}
