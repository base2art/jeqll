package com.base2art.jeqll.projections;

public class ByteField extends Field<Byte> implements IArithmaticField<Byte>
{

  public ByteField(String name)
  {
    super(name, Byte.class);
  }

  @Override
  public Byte add(Byte first, Byte other)
  {
    return (byte) ((byte) first + (byte) other);
  }

  @Override
  public Byte subtract(Byte first, Byte other)
  {
    return (byte) ((byte) first - (byte) other);
  }

  @Override
  public Byte multiply(Byte first, Byte other)
  {
    return (byte) ((byte) first * (byte) other);
  }

  @Override
  public Byte divideBy(Byte first, Byte other)
  {
    return (byte) ((byte) first / (byte) other);
  }

  @Override
  public Byte divideByInt(Byte first, int other)
  {
    return (byte) ((byte) first / other);
  }
}
