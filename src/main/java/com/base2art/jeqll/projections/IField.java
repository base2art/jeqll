package com.base2art.jeqll.projections;

public interface IField<T>
{

  String getName();

  Class<T> getKlazz();
}
