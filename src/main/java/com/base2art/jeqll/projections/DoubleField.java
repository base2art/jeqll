package com.base2art.jeqll.projections;

public class DoubleField extends Field<Double> implements IArithmaticField<Double>
{

  public DoubleField(String name)
  {
    super(name, Double.class);
  }

  @Override
  public Double add(Double first, Double other)
  {
    return first + other;
  }

  @Override
  public Double subtract(Double first, Double other)
  {
    return first - other;
  }

  @Override
  public Double multiply(Double first, Double other)
  {
    return first * other;
  }

  @Override
  public Double divideBy(Double first, Double other)
  {
    return first / other;
  }

  @Override
  public Double divideByInt(Double first, int other)
  {
    return first / other;
  }
}
