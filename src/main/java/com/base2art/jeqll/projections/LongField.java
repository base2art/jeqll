package com.base2art.jeqll.projections;

public class LongField extends Field<Long> implements IArithmaticField<Long>
{

  public LongField(String name)
  {
    super(name, Long.class);
  }

  @Override
  public Long add(Long first, Long other)
  {
    return first + other;
  }

  @Override
  public Long subtract(Long first, Long other)
  {
    return first - other;
  }

  @Override
  public Long multiply(Long first, Long other)
  {
    return first * other;
  }

  @Override
  public Long divideBy(Long first, Long other)
  {
    return first / other;
  }

  @Override
  public Long divideByInt(Long first, int other)
  {
    return first / other;
  }
}
