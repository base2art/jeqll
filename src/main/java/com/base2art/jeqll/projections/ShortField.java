package com.base2art.jeqll.projections;

public class ShortField extends Field<Short> implements IArithmaticField<Short>
{

  public ShortField(String name)
  {
    super(name, Short.class);
  }

  @Override
  public Short add(Short first, Short other)
  {
    return (short) (first + other);
  }

  @Override
  public Short subtract(Short first, Short other)
  {
    return (short) (first - other);
  }

  @Override
  public Short multiply(Short first, Short other)
  {
    return (short) (first * other);
  }

  @Override
  public Short divideBy(Short first, Short other)
  {
    return (short) (first / other);
  }

  @Override
  public Short divideByInt(Short first, int other)
  {
    return (short) (first / other);
  }
}
