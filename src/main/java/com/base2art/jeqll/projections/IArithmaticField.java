package com.base2art.jeqll.projections;

public interface IArithmaticField<T> extends IField<T>
{

  T add(T first, T other);

  T subtract(T first, T other);

  T multiply(T first, T other);

  T divideBy(T first, T other);

  T divideByInt(T first, int other);
}
