package com.base2art.jeqll.matchers;

public class StringContainsMatcher<TObject>
  extends MatcherBase<TObject, String>
{

  private final String value;
  private final boolean caseSensitive;

  public StringContainsMatcher(String column, String value, boolean caseSensitive)
  {
    super(column);
    this.value = value;
    this.caseSensitive = caseSensitive;
  }

  public StringContainsMatcher(String column, String value)
  {
    this(column, value, false);
  }

  @Override
  public boolean doIsMatch(String columnValue)
  {
    if (this.caseSensitive)
    {
      return columnValue.contains(this.value);
    }

    return columnValue.toUpperCase().contains(value.toUpperCase());
  }
}
