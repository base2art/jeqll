package com.base2art.jeqll.matchers;

public class SkipUntilMatcher implements IMatcher
{

  private final int i;

  public SkipUntilMatcher(int i)
  {
    this.i = i;
  }

  @Override
  public boolean isMatch(Object obj, int itemIndex)
  {
    return itemIndex >= i;
  }
}