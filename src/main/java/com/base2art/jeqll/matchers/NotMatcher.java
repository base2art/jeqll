package com.base2art.jeqll.matchers;

public class NotMatcher<TValue>
  implements IMatcher<TValue>
{

  private final IMatcher<TValue> backingMatcher;

  public NotMatcher(IMatcher<TValue> stringMatcher)
  {
    this.backingMatcher = stringMatcher;
  }

  @Override
  public boolean isMatch(TValue obj, int itemIndex)
  {
    return !this.backingMatcher.isMatch(obj, itemIndex);
  }
}
