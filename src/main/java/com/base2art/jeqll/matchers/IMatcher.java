package com.base2art.jeqll.matchers;

public interface IMatcher<TObject>
{

  boolean isMatch(TObject obj, int itemIndex);
}
