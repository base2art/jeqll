package com.base2art.jeqll.matchers;

public class TakeUntilMatcher implements IMatcher
{

  private final int i;

  public TakeUntilMatcher(int i)
  {
    this.i = i;
  }

  @Override
  public boolean isMatch(Object obj, int itemIndex)
  {
    return itemIndex < this.i;
  }
}
