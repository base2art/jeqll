package com.base2art.jeqll.matchers;

public class GreaterThanOrEqualMatcher<TObject, T extends Comparable<T>>
  extends MatcherBase<TObject, T>
{

  private final T value;

  public GreaterThanOrEqualMatcher(String column, T value)
  {
    super(column);
    this.value = value;
  }

  @Override
  public boolean doIsMatch(T obj)
  {
    return obj.compareTo(value) >= 0;
  }
}