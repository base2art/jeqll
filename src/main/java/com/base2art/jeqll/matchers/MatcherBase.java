package com.base2art.jeqll.matchers;

import com.base2art.jeqll.util.ReflectionUtil;

public abstract class MatcherBase<TObject, TValue> implements IMatcher<TObject>
{

  private final String column;

  protected MatcherBase(String column)
  {
    this.column = column;
  }

  protected TValue getColumnValue(Object obj)
  {
    return ReflectionUtil.<TValue>field(obj, this.column);
  }

  @Override
  public final boolean isMatch(TObject obj, int index)
  {
    return this.doIsMatch(this.getColumnValue(obj));
  }

  public abstract boolean doIsMatch(TValue columnValue);
}
