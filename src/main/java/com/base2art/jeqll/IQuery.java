package com.base2art.jeqll;

public interface IQuery<T>
  extends
  IRestrictableQuery<T>,
  IProjectableQuery<T>,
  IAggregableQuery<T>,
  IArithmeticAggregableQuery<T>,
  IConvertableQuery<T>,
  IGroupableQuery<T>,
  IPartionableQuery<T>,
  Iterable<T>
{

  T first();

  T last();

  T single();

  T elementAt(int i);
}
