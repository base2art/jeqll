package com.base2art.jeqll;

public interface IPartionableQuery<T>
{

  IQuery<T> take(int i);

  IQuery<T> skip(int i);
//    IPartitionedQuery<T> takeWhile(IPredicate<T> predicate);
//    IPartitionedQuery<T> skipWhile(IPredicate<T> predicate);
}
