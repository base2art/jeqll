package com.base2art.jeqll;

import com.base2art.jeqll.projections.IField;

public interface IProjectableQuery<T>
{

  <TProjected> IQuery<TProjected> select(IField<TProjected> convertor);

  <TProjected> IQuery<TProjected> selectMany(IField<TProjected> convertor);
}
