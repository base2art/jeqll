package com.base2art.jeqll;

import java.util.ArrayList;

public interface IConvertableQuery<T>
{

  T[] toArray();

  ArrayList<T> toList();
}
