package com.base2art.jeqll;

public interface IAggregableQuery<T>
{

  int count();
}
