package com.base2art.jeqll;

import com.base2art.jeqll.util.IPair;
import com.base2art.jeqll.projections.IField;

public interface IGroupableQuery<T>
{
//    <TKey> IGroupedQuery<TKey, T> groupBy(IField<TKey> field);

  <TKey extends Comparable<TKey>> IQuery<IPair<TKey, IQuery<T>>> groupBy(IField<TKey> field);
}
