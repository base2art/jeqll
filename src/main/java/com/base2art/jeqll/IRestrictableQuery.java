package com.base2art.jeqll;

import com.base2art.jeqll.matchers.IMatcher;

public interface IRestrictableQuery<T>
{

  IQuery<T> where(IMatcher matcher);
}
