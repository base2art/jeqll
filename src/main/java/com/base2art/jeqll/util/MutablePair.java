package com.base2art.jeqll.util;

import com.base2art.jeqll.util.IPair;

public class MutablePair<TKey, TValue> implements IPair<TKey, TValue>
{
  private TKey key;

  private TValue value;

  public MutablePair()
  {
  }

  public MutablePair(TKey key, TValue value)
  {
    this.key = key;
    this.value = value;
  }

  @Override
  public TKey getKey()
  {
    return key;
  }

  @Override
  public TValue getValue()
  {
    return value;
  }

  public void setKey(TKey key)
  {
    this.key = key;
  }

  public void setValue(TValue value)
  {
    this.value = value;
  }
}
