package com.base2art.jeqll.util;

import java.lang.reflect.Field;
import java.util.UnknownFormatConversionException;

public class ReflectionUtil
{

  public static <TValue> TValue field(Object obj, final String fieldName)
    throws UnknownFormatConversionException
  {
    try
    {
      Field f = obj.getClass().getField(fieldName);
      Object rez = f.get(obj);
      return (TValue) rez;
    }
    catch (ClassCastException | IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException ex)
    {
      throw new java.util.UnknownFormatConversionException(fieldName);
    }
  }
}
