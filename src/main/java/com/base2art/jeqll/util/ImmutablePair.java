package com.base2art.jeqll.util;

public class ImmutablePair<TKey, TValue> implements IPair<TKey, TValue>
{

  private final TKey key;
  private final TValue value;

  public ImmutablePair(TKey key, TValue value)
  {
    this.key = key;
    this.value = value;
  }

  @Override
  public TKey getKey()
  {
    return key;
  }

  @Override
  public TValue getValue()
  {
    return value;
  }
}
