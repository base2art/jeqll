package com.base2art.jeqll.util;

public interface IPair<TKey, TValue>
{
  TKey getKey();

  TValue getValue();
}
