package com.base2art.jeqll;

import com.base2art.jeqll.projections.IArithmaticField;
import com.base2art.jeqll.projections.IField;

public interface IArithmeticAggregableQuery<T>
{

  <TProjected extends Comparable<TProjected>> TProjected min(IField<TProjected> convertor);

  <TProjected extends Comparable<TProjected>> TProjected max(IField<TProjected> convertor);

  <TProjected> TProjected sum(IArithmaticField<TProjected> convertor);

  <TProjected> TProjected average(IArithmaticField<TProjected> convertor);
}
