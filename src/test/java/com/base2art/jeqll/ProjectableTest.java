package com.base2art.jeqll;

import com.base2art.jeqll.sampleData.TestObjectRepository;
import com.base2art.jeqll.sampleData.TestObject;
import com.base2art.jeqll.IQuery;
import com.base2art.jeqll.impl.IterableQuery;
import com.base2art.jeqll.matchers.EqualityMatcher;
import com.base2art.jeqll.projections.Field;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

public class ProjectableTest
{

  @Test
  public void shouldLoadProjectableSelect()
  {
    IQuery<TestObject> query = new IterableQuery<>(TestObjectRepository.createList(3), TestObject.class);
    IQuery<Integer> to = query.where(new EqualityMatcher<>("index", 2)).select(new Field<>("index", Integer.class));

    assertThat(to.first()).isEqualTo(2);
  }

  @Test
  public void shouldLoadProjectableSelectMany()
  {
    IQuery<TestObject> query = new IterableQuery<>(TestObjectRepository.createList(3), TestObject.class);
    IQuery<String> to = query
      .where(new EqualityMatcher<>("index", 2))
      .selectMany(new Field<>("tags", String.class));

    assertThat(to.elementAt(0)).isEqualTo("#tag 6");
    assertThat(to.elementAt(1)).isEqualTo("#tag 7");
    assertThat(to.elementAt(2)).isEqualTo("#tag 8");
    assertThat(to.count()).isEqualTo(3);

    query = new IterableQuery<>(TestObjectRepository.createList(3), TestObject.class);
    to = query
      .selectMany(new Field<>("tags", String.class));
    assertThat(to.count()).isEqualTo(9);
  }
}
