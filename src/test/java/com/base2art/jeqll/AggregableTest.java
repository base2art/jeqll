package com.base2art.jeqll;

import com.base2art.jeqll.IQuery;
import com.base2art.jeqll.impl.IterableQuery;
import com.base2art.jeqll.projections.DoubleField;
import com.base2art.jeqll.sampleData.SampleDataRepository;
import com.base2art.jeqll.sampleData.WeatherObservation;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

public class AggregableTest
{

  @Test
  public void shouldGetMinAndMax()
  {
    IQuery<WeatherObservation> query = new IterableQuery<>(SampleDataRepository.weatherObservations(), WeatherObservation.class);
    assertThat(query.max(new DoubleField("temp"))).isEqualTo(100.6);
    assertThat(query.min(new DoubleField("temp"))).isEqualTo(43.6);
  }

  @Test
  public void shouldGetAverageAndSum()
  {
    IQuery<WeatherObservation> query = new IterableQuery<>(SampleDataRepository.weatherObservations(), WeatherObservation.class);
    double actualSum = 43.6 + 44.6 + 45.6 + 46.6 + 47.6 + 100.6;
    assertThat(query.sum(new DoubleField("temp"))).isEqualTo(actualSum);
    assertThat(query.average(new DoubleField("temp"))).isEqualTo(actualSum / 6);
  }
}
