package com.base2art.jeqll;

import com.base2art.jeqll.IQuery;
import com.base2art.jeqll.util.IPair;
import com.base2art.jeqll.impl.IterableQuery;
import com.base2art.jeqll.projections.Field;
import com.base2art.jeqll.sampleData.WeatherObservation;
import com.base2art.jeqll.sampleData.SampleDataRepository;
import static org.fest.assertions.Assertions.assertThat;
import org.joda.time.ReadableInstant;
import org.junit.Test;

public class GroupableTest
{

  @Test
  public void shouldGetSimpleGroupBy()
  {
    IQuery<WeatherObservation> query = new IterableQuery<>(SampleDataRepository.weatherObservationDuplicates(), WeatherObservation.class);
    IQuery<IPair<ReadableInstant, IQuery<WeatherObservation>>> list =
      query.groupBy(new Field<>("time", ReadableInstant.class));

    assertThat(list.elementAt(0).getValue().count()).isEqualTo(1);
    assertThat(list.elementAt(1).getValue().count()).isEqualTo(4);
    assertThat(list.elementAt(2).getValue().count()).isEqualTo(1);
  }
}
