package com.base2art.jeqll;

import com.base2art.jeqll.SequenceContainsNoItemsException;
import com.base2art.jeqll.SequenceContainsNoItemsException;
import com.base2art.jeqll.sampleData.TestObject;
import com.base2art.jeqll.sampleData.TestObjectRepository;
import com.base2art.jeqll.impl.IterableQuery;
import java.util.ArrayList;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

public class QueryableTest
{

  @Test
  public void shouldGetFirstItem()
  {
    IterableQuery<TestObject> query= new IterableQuery<TestObject>(TestObjectRepository.createList(2), TestObject.class);

    TestObject testObject = query.first();
    assertThat(testObject).isNotNull();
    assertThat(testObject.getIndex()).isEqualTo(0);
  }


  @Test
  public void shouldGetLastItem()
  {
    IterableQuery<TestObject> query= new IterableQuery<TestObject>(TestObjectRepository.createList(2), TestObject.class);

    TestObject testObject = query.last();
    assertThat(testObject).isNotNull();
    assertThat(testObject.getIndex()).isEqualTo(1);
  }

  @Test(expected=SequenceContainsNoItemsException.class)
  public void shouldGetSingleItemWhereZero()
  {
    IterableQuery<TestObject> query= new IterableQuery<TestObject>(TestObjectRepository.createList(0), TestObject.class);
    query.single();
  }
  
  @Test
  public void shouldGetSingleItemWhereOne()
  {
    IterableQuery<TestObject> query= new IterableQuery<TestObject>(TestObjectRepository.createList(1), TestObject.class);
    TestObject testObject = query.single();
    assertThat(testObject).isNotNull();
    assertThat(testObject.getIndex()).isEqualTo(0);
  }

  @Test(expected=SequenceContainsNoItemsException.class)
  public void shouldGetSingleItemWhereTwo()
  {
    IterableQuery<TestObject> query= new IterableQuery<TestObject>(TestObjectRepository.createList(2), TestObject.class);
    query.single();
  }

  @Test
  public void shouldGetElementAtItemWhereInIndex()
  {
    IterableQuery<TestObject> query= new IterableQuery<TestObject>(TestObjectRepository.createList(4), TestObject.class);
    TestObject testObject = query.elementAt(2);
    assertThat(testObject).isNotNull();
    assertThat(testObject.getIndex()).isEqualTo(2);

    testObject = query.elementAt(3);
    assertThat(testObject).isNotNull();
    assertThat(testObject.getIndex()).isEqualTo(3);
  }

  @Test(expected=SequenceContainsNoItemsException.class)
  public void shouldGetElementAtItemWhereNotInIndex()
  {
    IterableQuery<TestObject> query= new IterableQuery<TestObject>(TestObjectRepository.createList(4), TestObject.class);
    TestObject testObject = query.elementAt(2);
    assertThat(testObject).isNotNull();
    assertThat(testObject.getIndex()).isEqualTo(2);

    testObject = query.elementAt(3);
    assertThat(testObject).isNotNull();
    assertThat(testObject.getIndex()).isEqualTo(3);

    query.elementAt(4);
  }
}
