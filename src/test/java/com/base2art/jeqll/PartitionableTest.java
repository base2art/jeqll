package com.base2art.jeqll;

import com.base2art.jeqll.IQuery;
import com.base2art.jeqll.impl.IterableQuery;
import com.base2art.jeqll.projections.Field;
import com.base2art.jeqll.sampleData.WeatherObservation;
import com.base2art.jeqll.sampleData.SampleDataRepository;
import static org.fest.assertions.Assertions.assertThat;
import org.joda.time.ReadableInstant;
import org.junit.Test;

public class PartitionableTest
{

  @Test
  public void shouldGetSkip()
  {
    IQuery<WeatherObservation> query = new IterableQuery<>(SampleDataRepository.weatherObservationDuplicates(), WeatherObservation.class);

    assertThat(query.skip(3).count()).isEqualTo(3);
    assertThat(query.skip(2).count()).isEqualTo(4);
    assertThat(query.take(2).count()).isEqualTo(2);
    assertThat(query.skip(2).take(2).count()).isEqualTo(2);
  }
}
