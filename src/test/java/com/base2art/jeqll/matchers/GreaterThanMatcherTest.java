package com.base2art.jeqll.matchers;

import com.base2art.jeqll.sampleData.TestObject;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;
import com.base2art.jeqll.matchers.GreaterThanMatcher;

public class GreaterThanMatcherTest 
{

  @Test
  public void shouldCompareCorrectly()
  {
    TestObject to = new TestObject("item1", 3);
    GreaterThanMatcher<TestObject, String> stringMatcher = new GreaterThanMatcher<>("name", "item1");
    GreaterThanMatcher<TestObject, Integer> intMatcher = new GreaterThanMatcher<>("index", 3);

    assertThat(stringMatcher.isMatch(to, 0)).isEqualTo(false);
    assertThat(intMatcher.isMatch(to, 0)).isEqualTo(false);

    stringMatcher = new GreaterThanMatcher<>("name", "itea");
    intMatcher = new GreaterThanMatcher<>("index", 2);
    assertThat(stringMatcher.isMatch(to, 0)).isEqualTo(true);
    assertThat(intMatcher.isMatch(to, 0)).isEqualTo(true);

    stringMatcher = new GreaterThanMatcher<>("name", "item4");
    intMatcher = new GreaterThanMatcher<>("index", 4);
    assertThat(stringMatcher.isMatch(to, 0)).isEqualTo(false);
    assertThat(intMatcher.isMatch(to, 0)).isEqualTo(false);
  }

  @Test
  public void shouldCompareCorrectlyDoIsMatch()
  {
    GreaterThanMatcher<TestObject, Integer> intMatcher = new GreaterThanMatcher<>("index", 3);
    assertThat(intMatcher.doIsMatch(3)).isEqualTo(false);
    assertThat(intMatcher.doIsMatch(2)).isEqualTo(false);
    assertThat(intMatcher.doIsMatch(4)).isEqualTo(true);
    //intMatcher.g
 }
}