package com.base2art.jeqll.matchers;

import com.base2art.jeqll.matchers.LessThanMatcher;
import com.base2art.jeqll.sampleData.TestObject;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;
import com.base2art.jeqll.matchers.GreaterThanMatcher;

public class LessThanMatcherTest
{

  @Test
  public void shouldCompareCorrectlyDoIsMatch()
  {
    LessThanMatcher<TestObject, Integer> intMatcher = new LessThanMatcher<>("index", 3);
    assertThat(intMatcher.doIsMatch(3)).isEqualTo(false);
    assertThat(intMatcher.doIsMatch(2)).isEqualTo(true);
    assertThat(intMatcher.doIsMatch(4)).isEqualTo(false);
  }
}