package com.base2art.jeqll.matchers;

import com.base2art.jeqll.matchers.EqualityMatcher;
import com.base2art.jeqll.sampleData.TestObject;
import static org.fest.assertions.Assertions.*;
import org.junit.Test;

public class EqualityMatcherTest
{

  @Test
  public void shouldCompareCorrectly()
  {
    TestObject to = new TestObject("item1", 3);
    EqualityMatcher<TestObject, String> stringMatcher = new EqualityMatcher<>("name", "item1");
    EqualityMatcher<TestObject, Integer> intMatcher = new EqualityMatcher<>("index", 3);
    assertThat(stringMatcher.isMatch(to, 0)).isEqualTo(true);
    assertThat(intMatcher.isMatch(to, 0)).isEqualTo(true);

    stringMatcher = new EqualityMatcher<>("name", "item2");
    intMatcher = new EqualityMatcher<>("index", 2);
    assertThat(stringMatcher.isMatch(to, 0)).isEqualTo(false);
    assertThat(intMatcher.isMatch(to, 0)).isEqualTo(false);
  }
}
