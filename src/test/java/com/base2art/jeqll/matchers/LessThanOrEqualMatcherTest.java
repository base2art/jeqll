package com.base2art.jeqll.matchers;

import com.base2art.jeqll.matchers.LessThanOrEqualMatcher;
import com.base2art.jeqll.sampleData.TestObject;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;
import com.base2art.jeqll.matchers.GreaterThanMatcher;

public class LessThanOrEqualMatcherTest
{

  @Test
  public void shouldCompareCorrectlyDoIsMatch()
  {
    LessThanOrEqualMatcher<TestObject, Integer> intMatcher = new LessThanOrEqualMatcher<>("index", 3);
    assertThat(intMatcher.doIsMatch(3)).isEqualTo(true);
    assertThat(intMatcher.doIsMatch(2)).isEqualTo(true);
    assertThat(intMatcher.doIsMatch(4)).isEqualTo(false);
  }
}