package com.base2art.jeqll.matchers;

//package com.mycompany.matchers.matchers;
//
//import com.mycompany.matchers.TestObject;
//import static org.fest.assertions.Assertions.*;
//import org.junit.Test;
//
//
//public class MatcherTestBase
//{
//
//  @Test
//  public void shouldCompareCorrectly()
//  {
//    TestObject to = new TestObject("item1", 3);
//    ComparableEqualityMatcher<TestObject, String> stringMatcher = new EqualityMatcher<>("name", "item1");
//    EqualityMatcher<TestObject, Integer> intMatcher = new EqualityMatcher<>("index", 3);
//    assertThat(stringMatcher.isMatch(to)).isEqualTo(true);
//    assertThat(intMatcher.isMatch(to)).isEqualTo(true);
//
//    stringMatcher = new EqualityMatcher<>("name", "item2");
//    intMatcher = new EqualityMatcher<>("index", 2);
//    assertThat(stringMatcher.isMatch(to)).isEqualTo(false);
//    assertThat(intMatcher.isMatch(to)).isEqualTo(false);
//  }
//}