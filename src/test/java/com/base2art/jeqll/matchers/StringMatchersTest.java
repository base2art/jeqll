package com.base2art.jeqll.matchers;


import com.base2art.jeqll.matchers.StringEndsWithMatcher;
import com.base2art.jeqll.matchers.StringContainsMatcher;
import com.base2art.jeqll.matchers.StringStartsWithMatcher;
import com.base2art.jeqll.sampleData.TestObject;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

public class StringMatchersTest
{


  @Test
  public void shouldCompareContainsCorrectlyDoIsMatch()
  {
    StringContainsMatcher<TestObject> intMatcher = new StringContainsMatcher<>("index", "em");
    assertThat(intMatcher.doIsMatch("item1")).isEqualTo(true);
    assertThat(intMatcher.doIsMatch("itEm1")).isEqualTo(true);
    assertThat(intMatcher.doIsMatch("hello")).isEqualTo(false);

    intMatcher = new StringContainsMatcher<>("index", "em", false);
    assertThat(intMatcher.doIsMatch("item1")).isEqualTo(true);
    assertThat(intMatcher.doIsMatch("iteM1")).isEqualTo(true);
    assertThat(intMatcher.doIsMatch("hello")).isEqualTo(false);

    intMatcher = new StringContainsMatcher<>("index", "em", true);
    assertThat(intMatcher.doIsMatch("item1")).isEqualTo(true);
    assertThat(intMatcher.doIsMatch("iteM1")).isEqualTo(false);
    assertThat(intMatcher.doIsMatch("hello")).isEqualTo(false);
    //intMatcher.g
 }


  @Test
  public void shouldCompareStartsWithCorrectlyDoIsMatch()
  {
    StringStartsWithMatcher<TestObject> intMatcher = new StringStartsWithMatcher<>("index", "start");
    assertThat(intMatcher.doIsMatch("startOfString")).isEqualTo(true);
    assertThat(intMatcher.doIsMatch("sTArtOfString")).isEqualTo(true);
    assertThat(intMatcher.doIsMatch("NotstartOfString")).isEqualTo(false);

    intMatcher = new StringStartsWithMatcher<>("index", "start", false);
    assertThat(intMatcher.doIsMatch("startOfString")).isEqualTo(true);
    assertThat(intMatcher.doIsMatch("sTArtOfString")).isEqualTo(true);
    assertThat(intMatcher.doIsMatch("NotstartOfString")).isEqualTo(false);

    intMatcher = new StringStartsWithMatcher("index", "start", true);
    assertThat(intMatcher.doIsMatch("startOfString")).isEqualTo(true);
    assertThat(intMatcher.doIsMatch("sTArtOfString")).isEqualTo(false);
    assertThat(intMatcher.doIsMatch("NotstartOfString")).isEqualTo(false);
    //intMatcher.g
 }


  @Test
  public void shouldCompareEndsWithCorrectlyDoIsMatch()
  {
    StringEndsWithMatcher<TestObject> intMatcher = new StringEndsWithMatcher<>("index", "end");
    assertThat(intMatcher.doIsMatch("Stringend")).isEqualTo(true);
    assertThat(intMatcher.doIsMatch("StringEnd")).isEqualTo(true);
    assertThat(intMatcher.doIsMatch("Stringending")).isEqualTo(false);

    intMatcher = new StringEndsWithMatcher<>("index", "end", false);
    assertThat(intMatcher.doIsMatch("Stringend")).isEqualTo(true);
    assertThat(intMatcher.doIsMatch("StringEnd")).isEqualTo(true);
    assertThat(intMatcher.doIsMatch("Stringending")).isEqualTo(false);

    intMatcher = new StringEndsWithMatcher<>("index", "end", true);
    assertThat(intMatcher.doIsMatch("Stringend")).isEqualTo(true);
    assertThat(intMatcher.doIsMatch("StringEnd")).isEqualTo(false);
    assertThat(intMatcher.doIsMatch("Stringending")).isEqualTo(false);
    //intMatcher.g
 }
}