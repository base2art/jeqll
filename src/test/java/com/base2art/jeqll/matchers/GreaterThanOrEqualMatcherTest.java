package com.base2art.jeqll.matchers;

import com.base2art.jeqll.matchers.GreaterThanOrEqualMatcher;
import com.base2art.jeqll.sampleData.TestObject;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;
import com.base2art.jeqll.matchers.GreaterThanMatcher;

public class GreaterThanOrEqualMatcherTest
{


  @Test
  public void shouldCompareCorrectlyDoIsMatch()
  {
    GreaterThanOrEqualMatcher<TestObject, Integer> intMatcher = new GreaterThanOrEqualMatcher<>("index", 3);
    assertThat(intMatcher.doIsMatch(3)).isEqualTo(true);
    assertThat(intMatcher.doIsMatch(2)).isEqualTo(false);
    assertThat(intMatcher.doIsMatch(4)).isEqualTo(true);
    //intMatcher.g
 }
}