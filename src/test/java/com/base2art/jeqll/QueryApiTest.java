package com.base2art.jeqll;

import com.base2art.jeqll.IQuery;
import com.base2art.jeqll.util.IPair;
import com.base2art.jeqll.impl.IterableQuery;
import com.base2art.jeqll.matchers.EqualityMatcher;
import com.base2art.jeqll.projections.Field;
import com.base2art.jeqll.sampleData.TestObject;
import com.base2art.jeqll.sampleData.TestObjectRepository;
import com.base2art.jeqll.sampleData.WeatherObservation;
import com.base2art.jeqll.sampleData.SampleDataRepository;
import static org.fest.assertions.Assertions.assertThat;
import org.joda.time.DateTime;
import org.joda.time.ReadableInstant;
import org.junit.Test;

public class QueryApiTest
{

  @Test
  public void shouldLoad()
  {
    IQuery<TestObject> query = new IterableQuery<>(TestObjectRepository.createList(3), TestObject.class);
    TestObject to = query.where(new EqualityMatcher<>("index", 0)).first();
    assertThat(to).isNotNull();
  }

  @Test
  public void shouldLoadProjectable()
  {
    IQuery<TestObject> query = new IterableQuery<>(TestObjectRepository.createList(3), TestObject.class);
    IQuery<Integer> to = query.where(new EqualityMatcher<>("index", 2)).select(new Field<>("index", Integer.class));

//    assertThat(to.count()).isEqualTo(3);
    assertThat(to.first()).isEqualTo(2);
  }

  @Test(expected = ClassCastException.class)
  public void shouldLoadProjectable2()
  {
    IQuery<TestObject> query = new IterableQuery<>(TestObjectRepository.createList(3), TestObject.class);
    IQuery<String> to = query.where(new EqualityMatcher<>("index", 2)).select(new Field<>("index", String.class));
    String obj = to.first();

//    assertThat(to).isEqualTo("test");
  }

  @Test
  public void shouldGetHighTempWeatherQuery()
  {
    IQuery<WeatherObservation> query = new IterableQuery<>(SampleDataRepository.weatherObservationDuplicates(), WeatherObservation.class);
    IQuery<IPair<ReadableInstant, IQuery<WeatherObservation>>> list = query.groupBy(new Field<>("time", ReadableInstant.class));

    for (IPair<ReadableInstant, IQuery<WeatherObservation>> pair : list)
    {
      System.out.println(pair.getKey());
      for (WeatherObservation wo : pair.getValue())
      {
        System.out.println(wo.getTemp());
      }
    }
  }
}
