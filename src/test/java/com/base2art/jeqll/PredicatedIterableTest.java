package com.base2art.jeqll;

import com.base2art.jeqll.sampleData.TestObjectRepository;
import com.base2art.jeqll.sampleData.TestObject;
import com.base2art.jeqll.impl.IterableQueryIterable;
import com.base2art.jeqll.matchers.EqualityMatcher;
import java.util.Iterator;
import static org.fest.assertions.Assertions.*;
import org.junit.Test;

public class PredicatedIterableTest
{

  @Test
  public void shouldGetSingleItemWhereOneMatch()
  {
    Iterable<TestObject> list = TestObjectRepository.createList(2);
    final EqualityMatcher matcher = new EqualityMatcher<>("index", 1);

    IterableQueryIterable<TestObject> iter = new IterableQueryIterable<TestObject>(list, matcher);
    final Iterator<TestObject> iterator = iter.iterator();
    assertThat(iterator.hasNext()).isEqualTo(true);
    TestObject testObject = iterator.next();
    assertThat(testObject).isNotNull();
    assertThat(testObject.getIndex()).isEqualTo(1);

    assertThat(iterator.hasNext()).isEqualTo(false);
  }


  @Test(expected=java.util.NoSuchElementException.class)
  public void shouldGetSingleItemWhereZeroMatch()
  {
    Iterable<TestObject> list = TestObjectRepository.createList(2);
    final EqualityMatcher matcher = new EqualityMatcher<>("index", 2);

    IterableQueryIterable<TestObject> iter = new IterableQueryIterable<TestObject>(list, matcher);
    final Iterator<TestObject> iterator = iter.iterator();
    assertThat(iterator.hasNext()).isEqualTo(false);
    iterator.next();
  }
}
