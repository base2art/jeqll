package com.base2art.jeqll.sampleData;

public class TestObject
{

  public String name;
  
  public int index;
  public String[] tags;

  public TestObject()
  {
  }

  public TestObject(String name, int index)
  {
    this.name = name;
    this.index = index;
    this.tags = new String[0];
  }

  public TestObject(String name, int index, String[] tags)
  {
    this.name = name;
    this.index = index;
    this.tags = tags;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public int getIndex()
  {
    return index;
  }

  public void setIndex(int index)
  {
    this.index = index;
  }

  public String[] getTags()
  {
    return tags;
  }

  public void setTags(String[] tags)
  {
    this.tags = tags;
  }
}
