package com.base2art.jeqll.sampleData;

import org.joda.time.DateTime;


public class WeatherObservation {
  public final DateTime time;
  public final double temp;

  public WeatherObservation(DateTime time, double temp)
  {
    this.time = time;
    this.temp = temp;
  }

  public double getTemp()
  {
    return temp;
  }

  public DateTime getTime()
  {
    return time;
  }

}
