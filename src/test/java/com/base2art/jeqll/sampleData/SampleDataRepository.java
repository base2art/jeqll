package com.base2art.jeqll.sampleData;

import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;

public class SampleDataRepository
{

  public static List<WeatherObservation> weatherObservations()
  {
    List<WeatherObservation> x = new ArrayList<>();
    x.add(new WeatherObservation(new DateTime(2013, 1, 1, 0, 0), 45.6));
    x.add(new WeatherObservation(new DateTime(2013, 2, 1, 0, 0), 46.6));
    x.add(new WeatherObservation(new DateTime(2013, 2, 2, 0, 0), 47.6));
    x.add(new WeatherObservation(new DateTime(2013, 2, 3, 0, 0), 43.6));
    x.add(new WeatherObservation(new DateTime(2013, 2, 4, 0, 0), 44.6));
    x.add(new WeatherObservation(new DateTime(2013, 6, 4, 0, 0), 100.6));
    return x;
  }

  public static List<WeatherObservation> weatherObservationDuplicates()
  {
    List<WeatherObservation> x = new ArrayList<>();
    x.add(new WeatherObservation(new DateTime(2013, 1, 1, 0, 0), 35.6));
    x.add(new WeatherObservation(new DateTime(2013, 2, 1, 0, 0), 46.6));
    x.add(new WeatherObservation(new DateTime(2013, 2, 1, 0, 0), 47.6));
    x.add(new WeatherObservation(new DateTime(2013, 2, 1, 0, 0), 43.6));
    x.add(new WeatherObservation(new DateTime(2013, 2, 1, 0, 0), 44.6));
    x.add(new WeatherObservation(new DateTime(2013, 6, 1, 0, 0), 100.6));
    return x;
  }

//  public static List<MailBox> mailBoxData()
//  {
//    List<MailBox> x = new ArrayList<>();
//    x.add(new WeatherObservation(new DateTime(2013, 1, 1, 0, 0), 35.6));
//    x.add(new WeatherObservation(new DateTime(2013, 2, 1, 0, 0), 46.6));
//    x.add(new WeatherObservation(new DateTime(2013, 2, 1, 0, 0), 47.6));
//    x.add(new WeatherObservation(new DateTime(2013, 2, 1, 0, 0), 43.6));
//    x.add(new WeatherObservation(new DateTime(2013, 2, 1, 0, 0), 44.6));
//    x.add(new WeatherObservation(new DateTime(2013, 6, 1, 0, 0), 100.6));
//    return x;
//  }
}
