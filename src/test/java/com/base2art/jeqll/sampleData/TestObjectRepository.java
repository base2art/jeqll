package com.base2art.jeqll.sampleData;

import com.base2art.jeqll.sampleData.TestObject;
import java.util.ArrayList;

public class TestObjectRepository
{

  public static Iterable<TestObject> createList(int i)
  {
    ArrayList<TestObject> items = new ArrayList<>();
    int tc = -1;
    for (int j = 0; j < i; j++)
    {
      final TestObject testObject = new TestObject("item" + j, j, new String[i]);

      for (int k = 0; k < i; k++)
      {
        tc += 1;
        testObject.tags[k] = "#tag " + tc;
      }

      items.add(testObject);
    }

    return items;
  }
}
