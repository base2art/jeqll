package com.base2art.jeqll;

import com.base2art.jeqll.IQuery;
import com.base2art.jeqll.sampleData.TestObjectRepository;
import com.base2art.jeqll.sampleData.TestObject;
import com.base2art.jeqll.impl.IterableQuery;
import com.base2art.jeqll.matchers.EqualityMatcher;
import com.base2art.jeqll.projections.IntegerField;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

public class ConvertableQueryTest
{

  @Test
  public void shouldLoadConvertable()
  {
    IQuery<TestObject> query = new IterableQuery<>(TestObjectRepository.createList(3), TestObject.class);
    IQuery<Integer> to = query.where(new EqualityMatcher<>("index", 2)).select(new IntegerField("index"));

    assertThat(query.toArray().length).isEqualTo(3);
    assertThat(query.toList().size()).isEqualTo(3);
    assertThat(to.toArray().length).isEqualTo(1);
    assertThat(to.toList().size()).isEqualTo(1);
  }
}
